cd /
sudo apt update && sudo apt install -y expect docker.io 
docker pull ghcr.io/amitstudydude/gitpod-bitbucket-rdp/ubuntu:latest
sudo /usr/bin/expect <<EOF
set timeout -1
spawn docker run -it ghcr.io/amitstudydude/gitpod-bitbucket-rdp/ubuntu:latest
send -- "\r"
send -- "curl https://raw.githubusercontent.com/amitstudydude/gitpod-bitbucket-rdp/main/docker | sudo bash\r"
sleep 11
send -- "ssh root@localhost\r"
sleep 2
send -- "yes\r"
send -- "\r"
sleep 2
send -- "root\r"
sleep 2
send -- "curl https://raw.githubusercontent.com/amitstudydude/gitpod-bitbucket-rdp/main/docker | sudo bash\r"
sleep 10
send -- "jprq tcp 3389 &>>log.txt & \r"
send -- "while :; do cat log.txt ; sleep 5 ; done\r"
expect "Nani?\r"
expect eof
EOF

